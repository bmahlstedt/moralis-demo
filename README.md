Can access the Moralis object in your client dapps.
Similar to the `ethereum` object or solana's `provider` object.

Can then:
* Connect to metamask
* Sign transactions
* Query user transaction history
* Subscribe for new transactions
* etc

You may also define cloud functions, stored on Moralis servers, that are callable from your dapps.

Moralis is crosschain; you may build apps that support eth, polygon, solana, and bsc.

To test: just serve `index.html` with live server in vscode.
It's connected to my Moralis eth server, but may be repointed to your own URL/ID.